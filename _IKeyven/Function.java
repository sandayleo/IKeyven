package _IKeyven;

import java.io.IOException;

import org.ansj.library.UserDefineLibrary;
import org.ansj.splitWord.analysis.ToAnalysis;

import _stsg.Learner;
import _stsg.Stsgcom;
import _trunk.DepRules;
import _trunk.DepTree;
import _trunk.Gotrunk;

public class Function {
	public static String compress(String str, String rulefile, boolean log) {
		if (log) {
			_fileutil.Log.log("_IKeyven.Function.compress");
		}
		Stsgcom sg = new Stsgcom();
		String temp = str, result = str;
		while (true) {
			temp = result;
			result = sg.compression(result, rulefile, log);
			if (temp.equals(result)) {
				break;
			}
		}
		;
		return result;
	}

	public static void learn(String corpustxt, String ruletxt) {
		_fileutil.Log.log("_IKeyven.Function.learn");
		Learner learner = new Learner();
		learner.learnRule(corpustxt, ruletxt);
	}

	public static void setModel(String model, String userwordstxt) {
		insertuserwords(userwordstxt);
		ToAnalysis.parse("");
		_fileutil.Log.log("_IKeyven.Function.setModel");
		_init.StanfordParser.getInstance().setModel(model);
	}

	public static void insertuserwords(String userwordstxt) {
		if (userwordstxt.trim().length() == 0) {
			return;
		}
		_fileutil.Log.log("_IKeyven.Function.insertuserwords");
		try {
			java.util.ArrayList<String> list = _fileutil.FileUtil.readFile(
					userwordstxt, "gbk");
			for (String str : list) {
				UserDefineLibrary.insertWord(str, "user's", 1000);
			}
		} catch (IOException e) {
			_fileutil.Log.log("用户自定义词读取失败!");
			_fileutil.Log.log(e.getMessage());
			e.printStackTrace();
		}
	}

	public static String trunkhankey(String str, String depruletxt, boolean log) {
		if (log) {
			_fileutil.Log.log("_IKeyven.Function.trunkhankey");
		}
		DepRules rulefile = new DepRules();
		rulefile.reader(depruletxt);
		DepTree tree = new DepTree(str);
		// System.out.println(tree.infoEdges());
		String result = Gotrunk.gotrunk(tree, rulefile, log);
		result = _trunk.Keyven.keyven(result, tree);
		return result;
	}
}
